var express     				= require('express'),
    app         				= express(),
    bodyParser					= require('body-parser'),
	 mongoose					= require('mongoose'),
	 userController 			= require('./server/controllers/user-controllers'),
	 friendsController 		= require('./server/controllers/friends-controllers');


// Connect to the database
mongoose.connect('mongodb://localhost:27017/json-messenger');

app.use(bodyParser());

app.get('/', function (req, res) {
    res.sendfile(__dirname + '/client/views/index.html');
});

// Routing files
app.use('/bootstrap', express.static(__dirname + '/client/bootstrap'));
app.use('/js', express.static(__dirname + '/client/js'));

// <==================================REST API================================================================>
// User Login
app.post('/api/login', userController.login);

// User Send Message
app.get('/api/messages', function (req, res) {
	// Insert the new message
	// Notify other user about the message
	res.sendfile(__dirname + '/client/views/messenger.html');
});

// Loads the users friends
app.post('/api/friends', friendsController.load);

// Server 
app.listen(3000, function () {
    console.log("Server is running...");
});

var mongoose = require('mongoose');

var user = new mongoose.Schema({
	name: {type: String, unique: true}
});

var friend = new mongoose.Schema({
	name: {type: String, unique: true},
	friends: [user],
});

var message = new mongoose.Schema({
	to: {type: String, unique: true},
	from: {type: String, unique: true},
	message: {type: String},
	sent: {type: Date, default: Date.now()},
});

var chat = new mongoose.Schema({
	users: [user],
	messages: [message],
});

module.exports = mongoose.model('chat', chat);
var user = new mongoose.Schema({
	username: { type: String, lowercase: true, unique: true},
	email: { type: String, lowercase: true, unique: true},
	password: String,
	is_active: {type: Boolean, default: false},
	devices: [{type: String, min: IPv4, max: IPv6}]
});

var friends = new mongoose.Schema({
	username: { type: String, lowercase: true, unique: true},
	friends: [user];
});

var message = new mongoose.Schema({
	username: { type: String, lowercase: true, unique: true},
	message: {type: String},
	sent: {type: Date, default: Data.now},
})

var chat = new mongoose.Schema({
	users: [user];
	messages: [message];
})
app.controller('loginController', ['$scope', '$window','$resource', 
	function ($scope, $window, $resource) {
		var User = $resource('/api/login');	
		
		$scope.login = function () {
			console.log($scope.userName);
			var user = new User;
			user.name = $scope.userName;
			user.$save(function (result) {
				if(result.answer == "true"){
					$window.location.href = '/api/messages' + "?user=" + $scope.userName;
				} else {
					// The incorrect username
					// Show error
				}	
			});
		}
}])